#!/usr/bin/env bash
# Copyright (c)  2023  Xiaomi Corporation
# Modified by Platinum Lucario (Davin Ockerby)

./setup_vcpkg_env.sh

export VCPKG_DIR=~/source/vcpkg
echo "Setting VCPKG_DIR environment variable to: 
    $VCPKG_DIR"

set -ex # This sets the shell to end if there's an error (e), and print out command args during execution (x)

rm -rf ../packages # The packages directory needs to be removed, so it can be made again

# Make sure all dependencies for cross compiling are installed before running this:
#     All Linux platforms:
#         clang
#         clang-tools
#     If on x86-64 Linux
#         gcc-aarch64-linux-gnu
#         g++-aarch64-linux-gnu
#         binutils-aarch64-linux-gnu
#     If on ARM64 Linux:
#         gcc-x86-64-linux-gnux32
#         g++-x86-64-linux-gnux32
#         binutils-x86-64-linux-gnu
# cpu="$(if [ -x "$(uname -p)" -eq 'ARM64' ]; then echo 'x86_64'; else echo 'aarch64')"
# ex="$(if [ -x "$(uname -p)" -eq 'ARM64' ]; then echo 'x32'; else echo '')"

# if [ -x "$(command -v apk)" ];  then sudo apk update && sudo apk upgrade && sudo apk add clang18 clang18-extra-tools gcc-cross-embedded g++-cross-embedded binutils
#     elif [ -x "$(command -v apt)" ];  then sudo apt update && sudo apt -y upgrade && sudo apt -y install clang clang-tools gcc-$cpu-linux-gnu$ex g++-$cpu-linux-gnu$ex binutils-$cpu-linux-gnu
#     elif [ -x "$(command -v dnf)" ];  then sudo dnf -y upgrade && sudo dnf -y install clang clang-tools-extra gcc-$cpu-linux-gnu gcc-c++-$cpu-linux-gnu binutils
#     elif [ -x "$(command -v zypper)" ];  then sudo zypper up && sudo zypper install -y clang clang-tools cross-$cpu-gcc14 cross-$cpu-binutils
#     elif [ -x "$(command -v pacman)" ];  then sudo pacman-key --refresh-keys && sudo pacman -Syuu --noconfirm && sudo pacman -S --noconfirm clang $cpu-linux-gnu-gcc $cpu-linux-gnu-binutils
#     elif [ -x "$(command -v nix)" ];  then nix-env --install clang clang-tools gcc binutils
#     else echo "No package manager found. The following packages must be installed manually:\nclang, clang-tools, gcc, binutils";
#     fi

./setup_libs_all.py # Updates and copies the libraries from VCPKG

# Update pip
python3 -m pip install --upgrade pip

# Install jinja2 if it hasn't already been installed
python3 -m pip install jinja2

./generate.py # Generates the project files, creates directories and copies the libraries

# Build and pack projects
# Linux x64
pushd ../projects/portaudio/linux
dotnet build -c Release portaudio.runtime.x64.csproj
dotnet pack -c Release portaudio.runtime.x64.csproj -o ../../../packages
popd
pushd ../projects/sndfile/linux
dotnet build -c Release sndfile.runtime.x64.csproj
dotnet pack -c Release sndfile.runtime.x64.csproj -o ../../../packages
popd
# Linux ARM64
pushd ../projects/portaudio/linux
dotnet build -c Release portaudio.runtime.arm64.csproj
dotnet pack -c Release portaudio.runtime.arm64.csproj -o ../../../packages
popd
pushd ../projects/sndfile/linux
dotnet build -c Release sndfile.runtime.arm64.csproj
dotnet pack -c Release sndfile.runtime.arm64.csproj -o ../../../packages
popd

# macOS x64
pushd ../projects/portaudio/macos
dotnet build -c Release portaudio.runtime.x64.csproj
dotnet pack -c Release portaudio.runtime.csproj -o ../../../packages
popd
pushd ../projects/sndfile/macos
dotnet build -c Release sndfile.runtime.x64.csproj
dotnet pack -c Release sndfile.runtime.x64.csproj -o ../../../packages
popd
# macOS ARM64
pushd ../projects/portaudio/macos
dotnet build -c Release portaudio.runtime.arm64.csproj
dotnet pack -c Release portaudio.runtime.csproj -o ../../../packages
popd
pushd ../projects/sndfile/macos
dotnet build -c Release sndfile.runtime.arm64.csproj
dotnet pack -c Release sndfile.runtime.arm64.csproj -o ../../../packages
popd

# Windows x64
pushd ../projects/portaudio/windows
dotnet build -c Release portaudio.runtime.x64.csproj
dotnet pack -c Release portaudio.runtime.x64.csproj -o ../../../packages
popd
pushd ../projects/sndfile/windows
dotnet build -c Release sndfile.runtime.x64.csproj
dotnet pack -c Release sndfile.runtime.x64.csproj -o ../../../packages
popd
# Windows ARM64
pushd ../projects/portaudio/windows
dotnet build -c Release portaudio.runtime.arm64.csproj
dotnet pack -c Release portaudio.runtime.arm64.csproj -o ../../../packages
popd
pushd ../projects/sndfile/windows
dotnet build -c Release sndfile.runtime.arm64.csproj
dotnet pack -c Release sndfile.runtime.arm64.csproj -o ../../../packages
popd
